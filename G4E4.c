//GUIA MANEJO STRINGS EJERCICIO 4
//El usuario ingresa una palabra.
//Mostrar en pantalla cuántas letras A minúsculas contiene.

#include <stdio.h>

int main()
{
    char A[20] = {0}; //Declaro las variables.
    int flag1 = 0;
    
    printf("Ingrese una palabra: "); //Solicito el ingreso de una palabra.
    scanf("%s", A);
    
    for (int i = 0; i<20; i++) //Recorro el string y sumo 1 al flag1 si el codigo ASCII de la letra i es 97 (A minúscula).
    {
        if (A[i] == 97)
        {
            flag1++;
        }
    }
    
    printf("La cantidad de A minúsculas es: %d", flag1); //Muestro la cantidad de A minúsculas que hay en la palabra ingresada.
    
    return 0;
}